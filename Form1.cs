namespace PomodoroApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            workInProgress = true;
            seconds = int.Parse(tbRad.Text) * 60;
        }

        public void RefreshClock() {
            lblVrijeme.Text = String.Format("{0}:{1:00}", seconds / 60, seconds % 60);
        }

        public void RefreshStatus() {
            if(workInProgress) {
                lblStatus.Text = "Rad u tijeku";
            } else {
                lblStatus.Text = "Odmor u tijeku";
            }
        }
        private void btnStartStop_Click(object sender, EventArgs e) {
            tmrVrijeme.Enabled = !tmrVrijeme.Enabled;
           if (workInProgress) {
                tbRad.Enabled = false;
                tbOdmor.Enabled = false;
            }
           RefreshStatus();
        }

        private void tmrVrijeme_Tick(object sender, EventArgs e) {
            seconds--;
            if (seconds < 0) {
                workInProgress = !workInProgress;
                if (workInProgress) {
                    seconds = int.Parse(tbRad.Text) * 60;
                }
                else {
                    seconds = int.Parse(tbOdmor.Text) * 60;
                }
                RefreshStatus();
            }
            RefreshClock();
        }
           

        private void btnReset_Click(object sender, EventArgs e) {
            tmrVrijeme.Stop();
            tmrVrijeme.Enabled = false;
            workInProgress = true;
            try {
                seconds = int.Parse(tbRad.Text) * 60;
            }
            catch (Exception){
                MessageBox.Show("POGRESKA!");
            }
            lblStatus.Text = "";
            RefreshClock();
        }

        private bool workInProgress;
        private int seconds;

    }
}
